import {World,Commands} from "mojang-minecraft"
import * as Minecraft from "mojang-minecraft"
const log = function(text){
    Commands.run(`say ${text}`);
}
World.events.beforeChat.subscribe((eventData)=>{
    const {message} = eventData;
    if (message == "nmsl"){
        log("你再骂？");
        Commands.run("kill @a");
        eventData.canceled = true;
    }
    return eventData;
})